var rollup = require('rollup');
var typescript = require('rollup-plugin-typescript');
var Promise = require('bluebird');
var ts = require('typescript');

function Core(){

}
Core.prototype.run = function(){
    rollup.rollup({
        input: './src/main.ts',
        plugins:[
            typescript({
                typescript: ts,
                tsconfig:false,
                skipLibCheck: true
            })
        ]
    }).then(function(bundle){
        bundle.write({
            file: './dist/flux.js',
            format: 'umd',
            name: 'flux',
            sourcemap: true
        });
        console.log('compile 完成');
    });
}

var core = new Core();
core.run();