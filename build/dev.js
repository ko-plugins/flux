var Koa = require('koa');
var static = require('koa-static');
var path = require('path');

var projDir = path.join(__dirname, '..');
var app = new Koa();
app.use(new static(path.join(projDir, 'dist')));
app.use(new static(path.join(projDir, 'test')));

app.listen(8001);
console.log('8001.......');