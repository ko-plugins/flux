(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.flux = {})));
}(this, (function (exports) { 'use strict';

function __extends(d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

/**
 * author: sam long
 * date: 2017-11-22
 * remark: 域的字典集合
 */
/**
 * 域字典
 */
var Dictionary = /** @class */ (function () {
    function Dictionary() {
    }
    return Dictionary;
}());

/**
 * author: sam long
 * date: 2017-11-29
 * question:
 *      1. 是否可以增加一个root属性，表示根状态器
 */
/**
 * 存储状态类
 */
var Store = /** @class */ (function () {
    function Store(opt) {
        this.entities = new Dictionary();
        this.getters = new Dictionary();
        this.opt = opt;
        this.state = opt.state;
        this.mapActions();
        this.mapGet();
    }
    Store.prototype.dispatch = function (key, payload) {
        var other = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            other[_i - 2] = arguments[_i];
        }
        return this.entities[key](payload, other);
    };
    Store.prototype.get = function (key) {
        return this.getters[key]();
    };
    Store.prototype.mapActions = function () {
        var actions = this.opt.actions;
        var that = this;
        var _loop_1 = function (key) {
            that.entities[key] = function wrapHandler(payload, other) {
                other.unshift(that.state, payload);
                return actions[key].apply(that, other);
            };
        };
        for (var key in actions) {
            _loop_1(key);
        }
    };
    Store.prototype.mapGet = function () {
        var getters = this.opt.getters;
        var that = this;
        var _loop_2 = function (key) {
            that.getters[key] = function wrapHandler() {
                return getters[key].call(that, that.state);
            };
        };
        for (var key in getters) {
            _loop_2(key);
        }
    };
    return Store;
}());

/**
 * 根状态存储器
 */
var RootStore = /** @class */ (function (_super) {
    __extends(RootStore, _super);
    function RootStore(opt) {
        var _this = _super.call(this, opt) || this;
        _this.areas = new Dictionary();
        return _this;
    }
    /**
     * 注册一个store到Area中
     * @param key 域的Id，相同Id会替换原有项
     * @param store 状态器
     */
    RootStore.prototype.register = function (key, store) {
        this.areas[key] = store;
    };
    /**
     *
     * @param key 移除一个域
     */
    RootStore.prototype.unRegister = function (key) {
        if (this.areas[key]) {
            delete this.areas[key];
        }
    };
    return RootStore;
}(Store));

/// <reference types="../types/main" />
var isRootStore = true;
var _ko;
/**
 * 创建一个状态存储器
 * @param opt state, actions相关配置
 */
function createStore(opt) {
    var _store = null;
    if (isRootStore) {
        _store = new RootStore(opt);
        isRootStore = false;
        bind(_store);
    }
    else {
        _store = new Store(opt);
    }
    return _store;
}
/**
 * 注入ko对象
 * @param ko
 */
function use(ko) {
    _ko = ko;
}
/**
 * 根Store和ko对象相互绑定
 * @param store 状态机
 */
function bind(store) {
    _ko.$store = store;
}
//auto install in dist mode
if (window && window.ko) {
    _ko = window.ko;
}

exports.createStore = createStore;
exports.use = use;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=flux.js.map
