
import Store from './store';
import IArea from '../interface/iarea';
import Dictionary from '../model/dictionary';
import StoreOpt from '../model/storeOpts';

/**
 * 根状态存储器
 */
class RootStore extends Store implements IArea {

    constructor(opt: StoreOpt){
        super(opt);
        this.areas = new Dictionary<Store>();
    }

    private areas: Dictionary<Store>;
    /**
     * 注册一个store到Area中
     * @param key 域的Id，相同Id会替换原有项
     * @param store 状态器
     */
    register(key:string, store: Store){
        if(!(store instanceof Store)){
            ReferenceError("store is not Store's object")
        }
        this.areas[key] = store;
    }
    /**
     * 
     * @param key 移除一个域
     */
    unRegister(key:string){
        if(this.areas[key]){
            delete this.areas[key];
        }
    }
}

export default RootStore;