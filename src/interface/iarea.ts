/**
 * author: sam long
 * date: 2017-11-22
 * remark: 状态机域的管理，域之间不能共享直接共享数据，可以说是一个隔离器
 */

 import Store from '../core/store';

 interface IArea{
     /**
      * 创建一个域
      * @key {string} 域名的惟一主键
      * 返回一个Area对象
      */
     register(key:string, store: Store );
     /**
      * 移除一个域
      */
     unRegister(key:string);
 }

 export default IArea;