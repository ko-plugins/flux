/**
 * author: sam long
 * date: 2017-11-22
 * remark: 状态存储器的接口
 */

 interface IStore{
     /**
      * 执行一个action
      */
     dispatch(key: string, payload:any);
     /**
      * 获取一个状态树上的状态对象
      */
     get(key:string)
 }

 export default IStore;