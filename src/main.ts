/// <reference types="../types/main" />

import Store from './core/store';
import RootStore from './core/rootStore';
import IStoreOpt from './model/storeOpts';

let isRootStore = true;
let StoreOpt: IStoreOpt;
let _ko;

/**
 * 创建一个状态存储器
 * @param opt state, actions相关配置 
 */
export function createStore(opt:IStoreOpt) : Store{
    var _store =null;
    if(isRootStore){
        _store = new RootStore(opt);
        isRootStore = false;
        bind(_store);
    }else{
        _store = new Store(opt);
    }
    return _store;
}

/**
 * 注入ko对象
 * @param ko 
 */
export function use(ko:any){
    _ko = ko;
}

/**
 * 根Store和ko对象相互绑定
 * @param store 状态机 
 */
function bind(store:any){
    _ko.$store = store;
}


//auto install in dist mode
if(window && window.ko){
    _ko = window.ko;
}




