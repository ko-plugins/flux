/**
 * author: sam long
 * date: 2017-11-22
 * remark: 域的字典集合
 */

/**
 * 域字典
 */
class Dictionary<T>{
    [index:string]: T;
}

export default Dictionary;