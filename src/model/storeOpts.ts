/**
 * author:sam long
 * date: 2017-11-24 
 * remark: 规范Store的输入配置参数
 */

interface StoreOption{
    /**
     * 状态机内存变量
     */
    state: any,
    /**
     * 可以改变状态机的方法集合
     */
    actions: any,
    /**
     * 获取数据的方法集合
     */
    getters: any
}

export default StoreOption;