define(['knockout'], function(ko) {

    function viewModel(){
        this.text = ko.computed(function(){
            return ko.$store.state.name() + '组件';
        });
    } 
 

    ko.components.register('detial',{
        viewModel:viewModel,
        template: '<h1 data-bind="text:text" ></h1>'
    });  
 
});