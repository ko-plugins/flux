requirejs.config({
    baseUrl: './',
    paths:{
        'knockout': './knockout-latest.debug',
        'flux': './flux'
    }
});

require(['require', 'flux', 'knockout'],function(require, flux, ko){
    
    var rootStore={
        state:{
            name: ko.observable('root'),
            component: ko.observable()
        },
        actions:{
            'setName':function(state, name){
                state.name(name);
            },
            'setComponent':function(state, component){
                state.component(component);
            }
        }
    }
    flux.use(ko);
    flux.createStore(rootStore);

    var vm = {
        name: ko.$store.state.name,
        newName: ko.observable(),
        component: ko.$store.state.component,
        loadModule:function(){
            require(['./one', './component'],function(one, component){
                ko.$store.dispatch('setComponent', 'detial');
            });
        },
        setName:function(){
            ko.$store.dispatch('setName', this.newName());
        }
    }

    var root = ko.applyBindings(vm, document.getElementById('app1'));

});